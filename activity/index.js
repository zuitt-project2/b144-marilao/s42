
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');



const fullname = () => {
	let fname = txtFirstName.value;
	let lname = txtLastName.value;

	spanFullName.innerHTML = `${fname} ${lname}`;
}

txtFirstName.addEventListener('keyup', fullname) 
txtLastName.addEventListener('keyup', fullname)

/*myFunction = function() {
    var first = document.getElementById("txt-first-name").value;
    var second = document.getElementById("txt-last-name").value;

    document.getElementById("span-full-name").innerHTML = first+" "+second;
}

spanFullName.addEventListener('keyup', myFunction);

function formSubmit() {
    document.forms["myForm"].submit();
}*/